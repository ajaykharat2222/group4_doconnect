import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AdminserviceService } from '../service/adminservice.service';

@Component({
  selector: 'app-adminregister',
  templateUrl: './adminregister.component.html',
  styleUrls: ['./adminregister.component.css']
})
export class AdminregisterComponent implements OnInit {
  constructor(private bar: MatSnackBar, private service: AdminserviceService) {}
  name: string = '';
  email: string = '';
  password: string = '';
  adminregister(): void {
    this.service
      .adminregister({
        name: this.name,
        email: this.email,
        password: this.password,
      })
      .then((res) => {
        if (res === 'ok') {
          this.name = '';
          this.email = '';
          this.password = '';
          this.bar.open('Registered Succesfully', 'Close', {
            duration: 3000,
          });
        } else {
          this.bar.open('Something went wrong please try again', 'Close', {
            duration: 3000,
          });
        }
      });
  }

  ngOnInit(): void {}
}
